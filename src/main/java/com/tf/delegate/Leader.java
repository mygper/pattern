package com.tf.delegate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 14:47
 */
public class Leader {

    private Map<String,IEmployee> register = new HashMap<>();

    public Leader(){
        register.put("加密",new Emp1());
        register.put("解密",new Emp2());
        register.put("架构",new Emp3());
    }

    public void command(String command) {
        register.get(command).doJob();
    }
}

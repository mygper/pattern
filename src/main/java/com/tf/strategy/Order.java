package com.tf.strategy;

import com.tf.strategy.pay.Payment;
import com.tf.strategy.pay.PaymentStrategy;
import lombok.Data;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:19
 */
@Data
public class Order {
    private String uid;
    private String orderId;
    private double amount;


    public Order(String uid,String orderId,double amount){
        this.uid = uid;
        this.orderId = orderId;
        this.amount = amount;
    }


    public RestMsg pay(){
        return pay(PaymentStrategy.DEFAULT_PAY);
    }

    public RestMsg pay(String key){
        Payment payment = PaymentStrategy.get(key);
        System.out.println("欢迎使用："+payment.getName()+"，本次交易金额为："+this.amount);
        RestMsg result = payment.pay(this.uid, this.amount);
        System.out.println(result);
        return result;
    }

}

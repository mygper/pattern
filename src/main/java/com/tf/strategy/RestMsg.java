package com.tf.strategy;

import lombok.Data;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:20
 */
@Data
public class RestMsg {
    private int code;
    private String msg;
    private Object data;


    public RestMsg() {
    }

    public RestMsg(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public RestMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

package com.tf.strategy;

import com.tf.strategy.pay.PaymentStrategy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:34
 */
public class PaymentTest {

    public static void main(String[] args) {

        Order order = new Order("1","1",200d);
        order.pay(PaymentStrategy.WECHAT_PAY);
    }
}

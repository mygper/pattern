package com.tf.strategy.pay;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:25
 */
public class AliPay extends Payment{
    @Override
    public String getName() {
        return "支付宝支付";
    }

    @Override
    public double getBalance(String uid) {
        return 800;
    }
}

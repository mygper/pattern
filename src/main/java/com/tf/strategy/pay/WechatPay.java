package com.tf.strategy.pay;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:25
 */
public class WechatPay extends Payment{
    @Override
    public String getName() {
        return "微信支付";
    }

    @Override
    public double getBalance(String uid) {
        return 10;
    }
}

package com.tf.strategy.pay;

import com.tf.strategy.RestMsg;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:22
 */
public abstract class Payment {

    public abstract String getName();

    public abstract double getBalance(String uid);

    public RestMsg pay(String uid,double amount){
        if(getBalance(uid) < amount){
            return new RestMsg(-1,"余额不足，交易失败！");
        }
        return new RestMsg(1,"支付成功");
    }
}

package com.tf.strategy.pay;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/15 16:27
 * @desc 用注册式单例实现策略模式选择
 */
public class PaymentStrategy {

    public static final String ALI_PAY = "AliPay";
    public static final String WECHAT_PAY = "WechatPay";
    public static final String DEFAULT_PAY = ALI_PAY;

    private PaymentStrategy(){}

    private static final Map<String,Payment> map = new HashMap();

    static {
        map.put(ALI_PAY,new AliPay());
        map.put(WECHAT_PAY,new WechatPay());
    }

    public static Payment get(String payKey){
        if(map.containsKey(payKey)){
            return map.get(payKey);
        }
        return map.get(DEFAULT_PAY);
    }
}

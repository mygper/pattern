package com.tf.singleton.register;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/12 16:28
 */
public class Container {

    private Container(){};
    private static final Map<String,Object> ioc = new ConcurrentHashMap<>();

    public Object getBean(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(!ioc.containsKey(className)){
            Class<?> clazz = Class.forName(className);
            ioc.put(className,clazz.newInstance());
        }
        return ioc.get(className);
    }
}

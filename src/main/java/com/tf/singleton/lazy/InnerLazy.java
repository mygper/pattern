package com.tf.singleton.lazy;

import java.io.Serializable;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/12 12:32
 * @desc 静态内部类
 */
public class InnerLazy implements Serializable {

    private InnerLazy(){
        if(LazyHolder.instance != null){
            throw new RuntimeException("单例被破坏！");
        }
    }

    public static InnerLazy getInstance(){
        return LazyHolder.instance;
    }

    private static class LazyHolder{
        private static final InnerLazy instance = new InnerLazy();
    }

    public Object readResolve(){
        return LazyHolder.instance;
    }

}

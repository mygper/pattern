package com.tf.singleton.lazy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/12 12:30
 */
public class SyncLazy {
    private static SyncLazy instance = null;

    private SyncLazy(){}

    public synchronized static SyncLazy getInstance(){
        if(instance == null){
            instance = new SyncLazy();
        }
        return instance;
    }
}

package com.tf.singleton.lazy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/12 12:30
 * @desc 双重检查锁
 */
public class DbCheckLazy {
    private static DbCheckLazy instance = null;

    private DbCheckLazy(){}

    public static DbCheckLazy getInstance(){
        if(instance == null){
            synchronized (DbCheckLazy.class){
                if (instance == null) {
                    instance = new DbCheckLazy();
                }
            }
        }
        return instance;
    }
}

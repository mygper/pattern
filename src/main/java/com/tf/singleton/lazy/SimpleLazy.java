package com.tf.singleton.lazy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/12 12:25
 */
public class SimpleLazy {

    private static SimpleLazy instance = null;

    private SimpleLazy(){}

    public static SimpleLazy getInstance(){
        if(instance == null){
            instance = new SimpleLazy();
        }
        return instance;
    }

}

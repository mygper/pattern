package com.tf.singleton.lazy;

import java.io.*;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/12 15:08
 */
public class SerializableTest {
    public static void main(String[] args) {
        InnerLazy lazy1;
        InnerLazy lazy2 = InnerLazy.getInstance();
        FileInputStream fis = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        try {
            fos = new FileOutputStream("ser.obj");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(lazy2);
            oos.flush();
            /******************************************************/
            fis = new FileInputStream("ser.obj");
            ois = new ObjectInputStream(fis);
            lazy1 = (InnerLazy) ois.readObject();


            System.out.println(lazy1);
            System.out.println(lazy2);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                fos.close();
                oos.close();
                fis.close();
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

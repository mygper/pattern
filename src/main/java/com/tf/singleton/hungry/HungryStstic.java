package com.tf.singleton.hungry;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 17:53
 */
public class HungryStstic {

    private static final HungryStstic instance;
    static {
        instance = new HungryStstic();
    }
    private HungryStstic(){}

    public static HungryStstic getInstance(){
        return instance;
    }
}

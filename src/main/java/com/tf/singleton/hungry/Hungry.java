package com.tf.singleton.hungry;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 17:53
 */
public class Hungry {

    private static final Hungry instance = new Hungry();

    private Hungry(){}

    public static Hungry getInstance(){
        return instance;
    }
}

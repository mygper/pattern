package com.tf.template;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/20 14:31
 */
public abstract class JdbcTemplate {

    private DataSource dateSource;

    public List<?> findAll(String sql,RowMapper rowMapper,Object... params) throws Exception {
        Connection conn = this.getConnection();
        PreparedStatement psmt = this.getStatment(conn,sql,params);
        ResultSet rs = psmt.executeQuery();
        List<?> list = parseResultSet(rs,rowMapper);
        return list;
    }

    private List<?> parseResultSet(ResultSet rs, RowMapper rowMapper) throws SQLException {
        List<Object> list = new ArrayList<>();
        int rowNum = 0;
        while (rs.next()){
            list.add(rowMapper.mapRow(rs,rowNum++));
        }
        return list;
    }

    protected final PreparedStatement getStatment(Connection conn, String sql, Object[] params) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(sql);
        if(params != null){
            for (int i = 0; i < params.length; i++) {
                ps.setObject(i,params[i]);
            }
        }
        return ps;
    }


    private final Connection getConnection() throws SQLException {
        return dateSource.getConnection();
    }
}

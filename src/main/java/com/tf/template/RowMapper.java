package com.tf.template;

import java.sql.ResultSet;
import java.util.List;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/20 14:39
 */
@FunctionalInterface
public interface RowMapper {
    <T> List<T> mapRow(ResultSet rs,int rowNum);
}

package com.tf.chain.auth.builderchain;


import com.tf.chain.auth.Member;

import java.util.Objects;

/**
 * Created by Tom.
 */
public class ValidateHandler extends Handler {
    public void doHandler(Member member) {
        if(Objects.isNull(member.getLoginName()) || Objects.isNull(member.getLoginPass())){
            System.out.println("用户名和密码为空");
            return;
        }
        System.out.println("用户名和密码不为空，可以往下执行");
        if(null != next) {
            next.doHandler(member);
        }
    }
}

package com.tf.decorator;

public abstract class BatterCakeDecorator extends BatterCake{

	private BatterCake batterCake;

	public BatterCakeDecorator(BatterCake batterCake) {

		this.batterCake = batterCake;
	}

	@Override
	protected String getName(){
		return batterCake.getName();
	}
	@Override
	protected int getPrice(){
		return batterCake.getPrice();
	}
}

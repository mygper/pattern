package com.tf.decorator;

public abstract class BatterCake {

	protected abstract String getName();

	protected abstract int getPrice();
}

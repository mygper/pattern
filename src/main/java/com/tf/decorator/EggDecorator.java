package com.tf.decorator;

public class EggDecorator extends BatterCakeDecorator{

	public EggDecorator(BatterCake batterCake) {
		super(batterCake);
	}

	@Override
	protected String getName(){
		return super.getName()+"+1个鸡蛋";
	}
	@Override
	protected int getPrice(){
		return super.getPrice() + 1 ;
	}
}

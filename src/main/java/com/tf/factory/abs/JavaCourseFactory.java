package com.tf.factory.abs;

import com.tf.factory.ICourse;
import com.tf.factory.INote;
import com.tf.factory.JavaCourse;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 17:31
 */
public class JavaCourseFactory extends AbstractCourseFactory {

    @Override
    ICourse createCourse() {
        return new JavaCourse();
    }

    @Override
    INote createNote() {
//        return new JavaNote();
        return null;
    }
}

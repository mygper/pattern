package com.tf.factory.abs;

import com.tf.factory.ICourse;
import com.tf.factory.INote;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 17:32
 */
public class AbsTest {
    public static void main(String[] args) {
        AbstractCourseFactory courseFactory = new JavaCourseFactory();
        ICourse course = courseFactory.createCourse();
        INote note = courseFactory.createNote();
        course.record();
//        note.write();
    }
}

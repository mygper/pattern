package com.tf.factory.abs;

import com.tf.factory.ICourse;
import com.tf.factory.INote;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 17:25
 * @desc 用abstract本意是打算把模板方法也接进来
 */
public abstract class AbstractCourseFactory {

    abstract ICourse createCourse();

    abstract INote createNote();
}

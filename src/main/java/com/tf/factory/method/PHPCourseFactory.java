package com.tf.factory.method;

import com.tf.factory.ICourse;
import com.tf.factory.PHPCourse;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 14:27
 */
public class PHPCourseFactory implements ICourseFactory{

    @Override
    public ICourse create() {
        return new PHPCourse();
    }
}

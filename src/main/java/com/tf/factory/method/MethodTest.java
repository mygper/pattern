package com.tf.factory.method;

import com.tf.factory.ICourse;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 14:28
 */
public class MethodTest {
    public static void main(String[] args) {
        ICourseFactory factory = new JavaCourseFactory();
        ICourse course = factory.create();
        course.record();
        factory = new PHPCourseFactory();
        course = factory.create();
        course.record();
    }
}

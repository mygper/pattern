package com.tf.factory.method;

import com.tf.factory.ICourse;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 14:26
 */
public interface ICourseFactory {

    ICourse create();

}

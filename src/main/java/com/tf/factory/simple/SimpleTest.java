package com.tf.factory.simple;

import com.tf.factory.ICourse;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 13:55
 */
public class SimpleTest {

    public static void main(String[] args) {
        SimpleFactory sf = new SimpleFactory();
        ICourse course = sf.create("PHP");
        course.record();
    }
}

package com.tf.factory.simple;

import com.tf.factory.ICourse;
import com.tf.factory.JavaCourse;
import com.tf.factory.PHPCourse;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/11 13:54
 */
public class SimpleFactory {

    public ICourse create(String name){
        if("java".equals(name)){
            return new JavaCourse();
        }else if ("PHP".equals(name)){
            return new PHPCourse();
        }else{
            return null;
        }
    }
}

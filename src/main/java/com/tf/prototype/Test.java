package com.tf.prototype;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/13 14:19
 */
public class Test {
    public static void main(String[] args) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        testshallow();
        System.out.println("____________________________________________");
        testDeep();
    }

    public static void testshallow() throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        PersonPO source = new PersonPO();
        source.setId(1);
        source.setName("tom");
        List srcHobbies = new ArrayList<String>();
        srcHobbies.add("hehe");
        srcHobbies.add("haha");
        source.setHobbies(srcHobbies);
        PersonVO target = new PersonVO();
        BeanUtil.shallowCopyProperties(source,target);
        target.getHobbies().set(0,"wolegequ");
        System.out.println(target);
        System.out.println(source.getHobbies());
        System.out.println(target.getHobbies());
        System.out.println(source.getHobbies() == target.getHobbies());
    }

    public static void testDeep() throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        PersonPO source = new PersonPO();
        source.setId(1);
        source.setName("tom");
        List srcHobbies = new ArrayList<String>();
        srcHobbies.add("hehe");
        srcHobbies.add("haha");
        source.setHobbies(srcHobbies);
        PersonVO target = (PersonVO) BeanUtil.deepCopyProperties(source,PersonVO.class);
//        target.getHobbies().set(0,"wolegequ");
        System.out.println(target);
        System.out.println(source.getHobbies());
        System.out.println(target.getHobbies());
        System.out.println(source.getHobbies() == target.getHobbies());
    }
}

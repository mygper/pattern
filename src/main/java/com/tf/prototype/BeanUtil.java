package com.tf.prototype;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/13 11:08
 */
public class BeanUtil {

    public static void shallowCopyProperties(Object source, Object target) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        Field[] fields = source.getClass().getDeclaredFields();
        PropertyDescriptor[] pds = new PropertyDescriptor[fields.length];
        for(int i=0;i<fields.length;i++){
            pds[i] = new PropertyDescriptor(fields[i].getName(),source.getClass());
        }
        Method readMethod,writeMethod;
        Object value;
        PropertyDescriptor pdt;
        for (PropertyDescriptor pd : pds) {
            readMethod = pd.getReadMethod();
            pdt = new PropertyDescriptor(pd.getName(),target.getClass());
            writeMethod = pdt.getWriteMethod();
            value = readMethod.invoke(source);
            writeMethod.invoke(target,value);
        }
    }

    public static Object deepCopyProperties(Object source,Class<?> targetClass){
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;

        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(source);

            bis = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bis);

            Object obj = ois.readObject();
            Object target = targetClass.newInstance();
            shallowCopyProperties(obj,target);
            return target;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                bos.close();
                oos.close();
                bis.close();
                ois.close();
            } catch (IOException e) {
            }
        }
        return null;
    }
}

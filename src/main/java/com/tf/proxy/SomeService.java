package com.tf.proxy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/14 10:26
 */
public interface SomeService {

    void doSomeThing();
}

package com.tf.proxy;

import com.tf.proxy.cglib.CglibProxy;
import com.tf.proxy.jdk.JdkProxy;
//import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/14 10:26
 */
public class SomeController {

    public static void main(String[] args) throws IOException {
        SomeController controller = new SomeController();
        controller.someJdkProxyRequest();
        byte[] bytes = null;//--ProxyGenerator.generateProxyClass("$Proxy0.class", new Class[]{SomeService.class});
        FileOutputStream fos = new FileOutputStream("E:/proxy.class");
        fos.write(bytes);
        fos.flush();
        fos.close();
        controller.someCglibProxyRequest();

    }

    private void someCglibProxyRequest() {
        SomeServiceImpl service = (SomeServiceImpl) new CglibProxy().getInstance(SomeServiceImpl.class);
        service.doSomeThing();
    }

    public void someJdkProxyRequest(){
        SomeService service = (SomeService) new JdkProxy().getInstance(new SomeServiceImpl());
        service.doSomeThing();
    }
}

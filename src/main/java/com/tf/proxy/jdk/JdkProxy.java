package com.tf.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/14 10:28
 */
public class JdkProxy implements InvocationHandler {
    private Object target;

    public Object getInstance(Object target){
        this.target = target;
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),target.getClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object obj = method.invoke(this.target,args);
        after();
        return obj;
    }

    private void before(){
        System.out.println("before invoke");
    }

    private void after(){
        System.out.println("after invoke");
    }
}

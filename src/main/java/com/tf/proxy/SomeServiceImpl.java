package com.tf.proxy;

/**
 * @author CREATE BY TENG FEI
 * @date 2019/3/14 10:27
 */
public class SomeServiceImpl implements SomeService{

    @Override
    public void doSomeThing() {
        System.out.println("处理某段业务逻辑");
    }
}
